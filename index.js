const mysql = require("mysql");
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "holamundo2021",
  database: "prueba2p",
});
connection.connect((error) => {
  if (error) {
    console.log("El error de conexion es : " + error);
    return;
  }
  console.log("Conectado a la base de datos!");
});

//1 .- Invocamos a express
const express = require("express");
const app = express();

//2 .- Seteamos urlencoded para capturar los datos del formulario
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//3 .- Establecemos el motor de plantillas ejs
app.set("view engine", "ejs");

//4 .- Invocamos req y res de express
const req = require("express/lib/request");
const res = require("express/lib/response");
const { redirect } = require("express/lib/response");

//5 .- Estableciendo las rutas
app.get("/", (req, res) => {
  res.render("registro", { error1: "",error2: "",error3: "",error4: "",error5: "",error6: "",error7: "", error8: "" });
});

//Crear una función JavaScript para guardar los datos de clientes
// (Cédula, nombres, dirección, teléfono, correo electrónico) usando PL/SQL. Validar los datos antes de guardarlos.

//Funcion JAVASCRIPT
app.post("/", async (req, res) => {
  //Expresiones regulares
  const expresiones = {
    cedula: /^\d{10}$/, // numérico de 10 dígitos.
    nombres: /^[a-zA-ZÀ-ÿ\s]{1,80}$/, //Texto de al menos 80 digitos.
    direccion: /^([a-zA-Z0-9_\s]){1,80}$/, //Alfanumerico de al menos 80 digitos.
    telefono: /^\d{10}$/, // numérico de 10 dígitos.
    correo: /\S+@\S+\.\S+/, // formato valido de correo.
  };
  //Asignar datos del formulario en la variable datos
  datos = req.body;
  //Validar los datos del formulario con las expresiones regulares
  var vacedula = expresiones.cedula.test(datos.form_cedula);
  var vanombres = expresiones.nombres.test(datos.form_nombres);
  var vadireccion = expresiones.direccion.test(datos.form_direccion);
  var vatelefono = expresiones.telefono.test(datos.form_telefono);
  var vacorreo = expresiones.correo.test(datos.form_correo);
  console.log(req.body);
  //Condiciones
  if (vacedula == true) {
    if (vanombres == true){
      if(vadireccion == true){
        if(vatelefono == true){
          if(vacorreo == true){
                //Metodo insertar cuando todo sea true
                connection.query("INSERT INTO datosclientes SET ?", {
                  bd_cedula: datos.form_cedula,
                  bd_nombres: datos.form_nombres,
                  bd_direccion: datos.form_direccion,
                  bd_telefono: datos.form_telefono,
                  bd_correo: datos.form_correo,
                });
                res.render("registro", { error6: "Datos Ingresados Correctamente", error1: "",error2: "",error3: "",error4: "",error5: ""});
          }else{
            res.render("registro", { error5: "Correo no valido.",  error1: "",error2: "",error3: "",error4: "", error6: ""});
          }
        }
        else{
          res.render("registro", { error4: "Telefono no valido.",  error1: "",error2: "",error3: "",error5: "",error6: ""});
        }
      }else{
        res.render("registro", { error3: "Direccion no valido.",  error1: "",error2: "",error4: "",error5: "",error6: ""});
      }
    }else{
      res.render("registro", { error2: "Nombres no valido.",  error1: "",error3: "",error4: "",error5: "",error6: ""});
    }
  } else {
    res.render("registro", { error1: "CI no valida.", error2: "",error3: "",error4: "",error5: "",error6: ""});
  }
});

app.listen(3000, (req, res) => {
  console.log("SERVER RUNNING IN http://localhost:3000");
});
